# My Métromobilité


## Contexte du projet

Réalisation d'un site internet responsive dont l’objectif est de pouvoir consulter les horaires de toutes les lignes des transports en commun de l’agglomération Grenobloise. Projet individuel effectué en deux semaines lors d'une formation.

Ce service comporte :

* une liste de toutes les lignes existantes avec leurs codes couleurs et leurs informations respectives (numéro de ligne / destination / code couleur etc)

* chaque catégorie de transport doit avoir son icône dédiée (bus / tram etc)

* le détail de chaque ligne qui contiendra toutes les informations utiles (horaires / arrêts / etc)

* une carte interactive qui présentera les informations suivantes :
    * le tracé de la ligne quand vous êtes sur sa fiche
    * Bonus : les différents arrêts de la ligne

* un système pour enregistrer vos lignes favorites (localstorage suffisant)

* un système d’annonces pour informer les usagers des règles sanitaires en vigueur : ce système devra se lancer au chargement de la page en tant que bandeau haut. Il y aura bien entendu la possibilité de le fermer mais il devra se ré-ouvrir au bout de quelques minutes.

Bonus : un système de notifications sur les informations de trafic en temps réel au chargement de la page


Bonus “Must Have” :

Au premier lancement de votre outil vous demandez des informations à votre utilisateur :

• son prénom, pour afficher un petit message avec par exemple “Bonjour Cédric”

• son genre (pour éventuellement adapter des éléments de design ou de contenu)

• choix d’un avatar dans une liste de choix prédéfinis

• géolocalisation pour afficher les points proches de sa position

• un dark-mode


## Moyens utilisés

* Utilisation de l’API de la métro : https://www.mobilites-m.fr/pages/opendata/OpenDataApi.html

* HTML5

* SASS

* jQuery avec Ajax, aucun framework additionnel

* Leaflet pour les cartes + geojson

* librairies installées avec NPM : Animate.css etc


## liens recettage:
 questionnaire: https://docs.google.com/forms/d/1t6jOUcYpNBCWBYvFXfeSlhvJgSoFa0VygnhASG17a9w/edit#responses

 réponse: https://docs.google.com/spreadsheets/d/1SH9smow_WxpumG0F-iCXhMk8Mxe4AwcGKASyYq1jLGs/edit#gid=1812549923
