const myMap = L.map('laCarte').setView([45.160038, 5.732706], 13);

let urlLignes = "http://data.metromobilite.fr/api/routers/default/index/routes";
let timestamp = Date.now();


$("document").ready(function(){

    //cacher le containerDetails
    $("#containerDetails").hide();
    $("#containerFavoris").hide();
    $("#alertCovid").addClass("animate__slideInDown");
    $("#alertCovid").show();
   
    search();

    $("#closeAlerte").click(function (){

        if($(".animate__slideOutUp") === true){
            $("#alertCovid").hide;
        } else {
            $("#alertCovid").removeClass("animate__slideInDown");
            $("#alertCovid").addClass("animate__slideOutUp");
        }
    })

    //ACCES A TOUTES LES LIGNES DE TRANSPORT
    //requête pour afficher la liste des lignes de transport
    function search(){

        $.ajax({
            url: urlLignes,
            type: "GET",
            dataType: "json",
        }).done(function(array){
            displayLines(array); // ma fonction perso
        // }).fail(function(error){
        //     console.warn('FAILLLLL');
        //     // console.log(error);
        // }).always(function(){
        //     console.warn('ALWAYS');
        //     // console.log('Terminé !');
        });
    }

    // fonction pour créer une liste
    function displayLines(array){
        $("#trams > .list").html('')
        $("#bus > .list").html('')
        $("#trains > .list").html('')

        for(let i=0 ; i < array.length; i++){
            let lines = array[i];

            //éléments que je veux récupérer
            let shortName = lines.shortName;
            let longName = lines.longName;
            let myColor = "#" + lines.color;
            let id = lines.id;
            let mode = lines.mode;
            const li = `<li class="lineName" style="background-color:${myColor};" data-id="${id}" data-name="${longName}" data-number="${shortName}" data-color="${myColor}" data-mode="${mode}">${shortName}</li>`;

            switch(mode){
                case "TRAM":
                    $("#trams > .list").append(li);
                break;
                case "BUS":
                    $("#bus > .list").append(li);
                break;
                case "RAIL":
                    $("#trains > .list").append(li);
                break;
            };
        };

        // évènement au click pour stocker des données dans le sessionStorage
        $("li").click((event) => {
            let lineName = $(event.target).data("name");
            sessionStorage.setItem("name", lineName);

            let lineNumber = $(event.target).data("number");
            sessionStorage.setItem("number", lineNumber);

            let lineId =  $(event.target).data("id");
            sessionStorage.setItem("id", lineId);
            
            let lineColor = $(event.target).data("color");
            sessionStorage.setItem("color", lineColor);
            
            let lineMode = $(event.target).data("mode");
            sessionStorage.setItem("mode", lineMode);
        });

        // évènement au click pour afficher containerDetails
        $("li").click(function fadeInOut(){
            $("#containerLines").fadeOut(1000);
            $("#containerDetails").fadeIn(2000);

            displayInfos();
            displayMap();
        });
    };

   //DETAILS POUR UNE LIGNE SÉLECTIONÉE

    //récupérer les data de chaque lignes et les afficher
    function displayInfos(){
        //requête qui récupère les infos
        let myId =  sessionStorage.getItem("id");
        let myUrl = `http://data.metromobilite.fr/api/ficheHoraires/json?route=${myId}&time=${timestamp}`;
        
        $.ajax({
            url: myUrl,
            type: "GET",
            dataType: "json",
        }).done(function(dataD){
            showDetails(dataD);
        });
    };

   
    
    
    function showDetails(dataD){
        
        //récupération data sessionStorage
        const dataNumber = sessionStorage.getItem("number");
        const dataColor = sessionStorage.getItem("color");
        const dataName = sessionStorage.getItem("name");
        const dataId = sessionStorage.getItem("id");
        const dataMode = sessionStorage.getItem("mode");
        console.log(dataMode)

        //afficher le numéro de la ligne
        $("#lineNumber").html(`
        <div class="number" style="background-color:${dataColor}; data-name="${dataName}">${dataNumber}</div>`);


        //afficher le mode de transport
        switch(dataMode){
            
            case "TRAM":
                $("#lineNumber").append("<p></p>");
                $("p").text("("+ "tram" + ")");
            break;
            case "BUS":
                $("#lineNumber").append("<p></p>");
                $("p").text("("+ "bus" + ")");
            break;
            case "RAIL":
                $("#lineNumber").append("<p></p>");
                $("p").text("("+ "train" + ")");
            break;
        };

        
        // afficher l'itinéraire

        // Trajet 1
        let pathA = dataD[0];
        let pathB = dataD[1];

        $("#allerA").html(`${pathA.arrets[0].stopName}`);
        $("#retourA").html(`${pathB.arrets[0].stopName}`);
        
        const stopsA = pathA.arrets;
            
        for (let i = 0; i < stopsA.length; i++) {
            const stopA = stopsA[i];
            const timesA = stopA.trips;
            const stopName = stopA.stopName;
            const stopCode = stopA.parentStation.code;
            const tr = $(`<tr data-arr-code="${stop.stopId}" data-parent-station="${stopCode}"><td>${stopName}</td></tr>`);
            
            //afficher le nom de chaque arrêt dans le tableau
            $("#tabA > tbody").append(tr);

            
            //afficher les horaires
            for (let i = 0; i < timesA.length; i++){
                let timeA = timesA[i];

                if (timeA === undefined){
                    const td = `<td class="timeA">||</td>`;
                    tr.append(td);

                } else{
                    time = convertSecToHrsMins(timeA);
                    
                    // fonction pour convertir les horaires 
                    function convertSecToHrsMins(timeA) {
                        timeA += 7200;
                        let h = Math.floor(timeA / 3600);
                        timeA -= h*3600;
                        let m = Math.floor(timeA / 60);
                        timeA -= m*60;
                        return h+"h"+(m < 10 ? '0'+m : m);
                    }; 
                    
                    const td = `<td class="time">${time}</td>`;
                    tr.append(td);
                };
            };

        };


        // Trajet 2
        $("#allerB").html(`${pathB.arrets[0].stopName}`);
        $("#retourB").html(`${pathA.arrets[0].stopName}`);
         
        const stopsB = pathB.arrets;
             
        for (let i = 0; i < stopsB.length; i++) {
            const stopB = stopsB[i];
            const timesB = stopB.trips;
            const stopNameB = stopB.stopName;
            const stopCodeB = stopB.parentStation.code;
            const tr = $(`<tr data-arr-code="${stop.stopId}" data-parent-station="${stopCodeB}"><td>${stopNameB}</td></tr>`);
            
            //afficher le nom de chaque arrêt dans le tableau
            $("#tabB > tbody").append(tr);
 
             
            //afficher les horaires
            for (let i = 0; i < timesB.length; i++){
                let timeB = timesB[i];
 
                if (time === undefined){
                    const td = `<td class="time">||</td>`;
                    tr.append(td);
                } else{
                    time = convertSecToHrsMins(timeB);

                    // fonction pour convertir les horaires 
                    function convertSecToHrsMins(timeB) {
                        timeB += 7200;
                        let h = Math.floor(timeB / 3600);
                        timeB -= h*3600;
                        let m = Math.floor(timeB / 60);
                        timeB -= m*60;
                        return h+"h"+(m < 10 ? '0'+m : m);
                    };
                     
                    const td = `<td class="time">${time}</td>`;
                    tr.append(td);
                };
            };
        }; 
    };
        
    //requête pour récupérer le tracer de l'itinéraire
    function displayMap(){
        const dataId = sessionStorage.getItem("id");

        $.ajax({
            url: `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${dataId}`,
            type: "GET",
            dataType: "json",
        }).done(function(dataM){

            showMap(dataM);
        });
    };
        
    //afficher la carte avec le tracé de la ligne
    function showMap(dataM){
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(myMap);
        
        const geoJsonLine = dataM; 

        const dataColor = sessionStorage.getItem("color");
       
        
        L.geoJSON(geoJsonLine, {
            style: {
                color: dataColor
            }
        }).addTo(myMap);
    };
    
        
    //retour à la liste des lignes
    $("#return").click( function returnList(){
        $("#containerDetails").fadeOut(1000);
        $("#containerFavoris").fadeOut(1000);
        $("#containerLines").fadeIn(2000);
        search();



        //clear session storage
        sessionStorage.clear();
    });
        
    //MES FAVORIS

    // afficher page favoris
    $("#favoris").click(function fadeInFavoris(){
        $("#containerLines").fadeOut(1000);
        $("#containerDetails").fadeOut(1000);
        $("#containerFavoris").fadeIn(2000);
     
        displayFavoris();
    });

    
    //AJOUTER AUX FAVORIS
    $("#addFav").click( function addToFavoris(){

        //récupération data sessionStorage
        const dataNumber = sessionStorage.getItem("number");
        const dataColor = sessionStorage.getItem("color");
        const dataName = sessionStorage.getItem("name");
        const dataId = sessionStorage.getItem("id");
        const dataMode = sessionStorage.getItem("mode");


        localStorage.key("myFavs");

        if(localStorage.getItem("myFavs") === null){
            let myBestLinesTab = [];
            let lineInfo = {
                id: dataId,
                number: dataNumber,
                name: dataName,
                color: dataColor,
                mode: dataMode
            };

            myBestLinesTab.push(lineInfo);
            JSON.stringify(myBestLinesTab);    
            localStorage.setItem("myFavs", JSON.stringify(myBestLinesTab));

            //afficher une alerte 
            alert("votre ligne a été ajoutée avec succès!");
        }else{

            //afficher une alerte 
            alert("votre ligne a déjà été ajoutée!");

            // myFavTab = JSON.parse(localStorage.getItem("myFavs"));
            // myBestLinesTab = myFavTab

            // if(myBestLinesTab.includes(`${dataNumber}`)){

            //     alert("cetteligne est déjà dans vos favoris");
            // }else {
                
            //     let lineInfo = {
            //         id: dataId,
            //         number: dataNumber,
            //         name: dataName,
            //         color: dataColor,
            //         mode: dataMode
            //     };
    
            //     myBestLinesTab.push(lineInfo);
            //     JSON.stringify(myBestLinesTab);    
            //     localStorage.setItem("myFavs", JSON.stringify(myBestLinesTab));
    
                
            //}
        }
        
        
    });

    //afficher les favoris
    function displayFavoris(){

        //const name = infos.number;
        let myFavTab = JSON.parse(localStorage.getItem("myFavs"));

        for (i in localStorage) {
            const favLine = myFavTab[i];
            console.log(favLine);
            // const LineTab = favLine.color;
            // console.log(LineTab)

            const btn = '<button>supprimer</button>';
        
            const div = $(`<div id="lineNumber" class="${favLine.id}">
            <div class="number" style="background-color:${favLine.color};">
                ${favLine.number}
            </div>
            <p>${favLine.name}</p>
            
            </div>`);
            $("#favList").append(div);
            $(div).append(btn);
                    
            //retirer un favoris de la liste
            $("#lineNumber" > btn).click(function(){
                $("#lineNumber").remove();
            });
        }    
    }

   
  
    
});